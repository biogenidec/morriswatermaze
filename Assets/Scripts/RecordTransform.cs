﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; //This allows the IComparable Interface
using System.Linq;
using System.Text;
using System.IO; 


public class RecordTransform : MonoBehaviour
{
    public string fileName = "defaultName";
    public float dataFreq =1f;
    private float tempTime = 0f; 

    private GameObject gO;
    private Transform gOT;

    private List<InfoMove> infoMoves;
    private int entryCounter = 0;

    public bool collectData = false;
    private bool checkCollectData; 
    public bool printBool = false;
    public bool clearList = false;
    public bool writeData = false; 

    // Use this for initialization
	void Start ()
    {
        gO = this.gameObject;
        gOT = gO.GetComponent<Transform>();

        //Declare infoMoves
        infoMoves = new List<InfoMove>();

        //Test addition to list; 
        //infoMoves.Add(new InfoMove(0, 1f, 2f, 3f, 4f, 5f, 6f));

        checkCollectData = collectData;

        //Find out where to look for the file that may be created
        print(Application.persistentDataPath);
        
	}
	
	// Update is called once per frame
	void Update ()
    {

        //Set tempTime = 0 if collectData has changed
        if (checkCollectData != collectData)
        {
            tempTime = 0f;
            checkCollectData = collectData; 
        }
        
        //Track time during data collection
        if (collectData == true)
        {
            //Increment time counter
            tempTime += Time.deltaTime;
        }

        //Trigger statement when tempTime >= dataFreq
        if(tempTime >= dataFreq)
        {
            //Reset tempTime; 
            tempTime = 0f;

            //Add an entry to the list
            infoMoves.Add(new InfoMove(entryCounter, gOT.position.x, gOT.position.y, gOT.position.z, gOT.rotation.x, gOT.rotation.y, gOT.rotation.z));

            Debug.Log("Entry " + entryCounter + " Recorded");

            //Increase entry counter
            entryCounter += 1; 
        }

        if(printBool == true)
        {  
            //Print the values in the console
            foreach (InfoMove entry in infoMoves)
            {
                print("Count: " + entry.indexInt + " X Position: " + entry.posX);
            }

            //set printBool to false
            printBool = false; 
        }

        //Clear the list and reset the counter
        if (clearList == true)
        {
            infoMoves.Clear();
            entryCounter = 0; 

            clearList = false; 

        }

        //Write data to CSV file
        if(writeData == true)
        {
            print("Writing Data");
            
            //Set the file name and location
            StreamWriter sw = new StreamWriter(Application.persistentDataPath +"/" + fileName + ".csv");

            //Create header
            sw.WriteLine("Index" + ", " + "x-position" + ", " + "y-position" + ", " + "z-position" + ", " + "x-rotation" + ", " + "y-rotation" + ", " + "z-rotation");

            //Write the lines
            foreach (InfoMove entry in infoMoves)
            {
               sw.WriteLine(entry.indexInt + ", " + entry.posX + ", " + entry.posY + ", " + entry.posZ + ", " + entry.rotX + ", " + entry.rotY + ", " + entry.rotZ);
            }

            //End the file writing process
            sw.Close(); 

            //set writeData to false
            writeData = false; 
        }

	}

    public class InfoMove : IComparable<InfoMove>
    {
        public int indexInt; 
        public float posX;
        public float posY;
        public float posZ;
        public float rotX;
        public float rotY;
        public float rotZ; 
        
        public InfoMove(int recentIndex, float recentPosX, float recentPosY, float recentPosZ, float recentRotX, float recentRotY, float recentRotZ)
        {
            indexInt = recentIndex; 
            posX = recentPosX;
            posY = recentPosY;
            posZ = recentPosZ;
            rotX = recentRotX;
            rotY = recentRotY;
            rotZ = recentRotZ; 
        }
        
        //This method is required by the IComparable interface
        public int CompareTo(InfoMove other)
        {
            if(other == null)
            {
                return 1; 
            }

            return indexInt - other.indexInt; 
        }
    }
}
