﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateByArrows : MonoBehaviour
{
    public float rotationSpeed = 1f; 

    private GameObject gO;
    private Transform gOT; 
    
    // Use this for initialization
	void Start ()
    {
        gO = this.gameObject;
        gOT = gO.GetComponent<Transform>(); 
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Rotate object based on keyboard arrows
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            gOT.Rotate(-Vector3.up * rotationSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            gOT.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            gOT.Rotate(-Vector3.right * rotationSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            gOT.Rotate(Vector3.right * rotationSpeed * Time.deltaTime);
        }
    }
}
