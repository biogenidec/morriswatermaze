﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportByMouseClick : MonoBehaviour
{
    public Transform moveThis;
    public Camera currentCamera; 

    public LayerMask hitLayers; 
	
    // Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            Vector3 mouse = Input.mousePosition;
            Ray castPoint = currentCamera.ScreenPointToRay(mouse);
            RaycastHit hit; 

            if(Physics.Raycast(castPoint, out hit, Mathf.Infinity, hitLayers))
            {
                moveThis.transform.position = new Vector3 (hit.point.x, moveThis.transform.position.y, hit.point.z); 
            }
        }
    }
}
