﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTrigger : MonoBehaviour
{

    public GameObject targetObject;
    public GameObject targetFlag; 
    
    // Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == targetObject)
        {
            //Activate object to demonstrate that the target has been found
            targetFlag.SetActive(true);

            Debug.Log("Found it!");
        }
    }
}
